import { requestAuth } from "../requestAuth"

const verifyEmailService = {
  verifyEmail: (sms_id, otp, data) =>
    requestAuth.post(`v2/verify-email/${sms_id}/${otp}`, data),
  register: (data) => requestAuth.post("v2/register-email-otp/user", data),
}

export default verifyEmailService
