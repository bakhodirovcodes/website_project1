import { requestAuth } from "./requestAuth"
import { useMutation } from "react-query"

const verifyEmailService = {
  sendCode: (data) => requestAuth.post(`v2/send-message`, data),
}

export const useSendCodeMutation = (mutationSettings) => {
  return useMutation(
    (data) => verifyEmailService.sendCode(data),
    mutationSettings
  )
}
