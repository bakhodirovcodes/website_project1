import ProfileLayout from "@/components/ProfileLayout/ProfileLayout"
import ReceivedView from "@/components/views/Profile/MyParcels/ReceivedView"

function Received() {
  return (
    <ProfileLayout>
      <ReceivedView />
    </ProfileLayout>
  )
}

export default Received
