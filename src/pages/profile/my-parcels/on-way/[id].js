import ProfileLayout from "@/components/ProfileLayout/ProfileLayout"
import AddedSingleView from "@/components/views/Profile/MyParcels/AddedSingleView"

function OnWaySingle() {
  return (
    <ProfileLayout>
      <AddedSingleView />
    </ProfileLayout>
  )
}

export default OnWaySingle
