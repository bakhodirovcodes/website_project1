import ProfileLayout from "@/components/ProfileLayout/ProfileLayout"
import RecipientsView from "@/components/views/Profile/RecipientsView"

function Recipients() {
  return (
    <ProfileLayout>
      <RecipientsView />
    </ProfileLayout>
  )
}

export default Recipients
