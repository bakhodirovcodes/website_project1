import ProfileLayout from "@/components/ProfileLayout/ProfileLayout"
import BalanceAndTransactionsView from "@/components/views/Profile/BalanceAndTransactionsView"

function BalanceAndTransactions() {
  return (
    <ProfileLayout>
      <BalanceAndTransactionsView />
    </ProfileLayout>
  )
}

export default BalanceAndTransactions
