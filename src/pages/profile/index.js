import ProfileLayout from "@/components/ProfileLayout/ProfileLayout";

function Profile() {
  return <ProfileLayout />;
}

export default Profile;
