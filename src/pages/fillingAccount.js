import SEO from "@/components/SEO"
import FillingAccount from "@/components/views/FillingAccount/FillingAccount"

export default function fillingAccount() {
  return (
    <>
      <SEO />
      <FillingAccount />
    </>
  )
}
