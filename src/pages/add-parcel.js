import React from "react";
import SEO from "@/components/SEO";
import AddParcelPage from "@/components/views/AddParcel/AddParcel";

const AddParcel = () => {
  return (
    <>
      <SEO />
      <AddParcelPage />
    </>
  );
};

export default AddParcel;
