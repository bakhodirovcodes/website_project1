import SEO from "@/components/SEO"
import PrivacyLink from "@/components/UI/Links/PrivaceLink/PrivacyLink"

export default function PrivacyPolice() {
  return (
    <>
      <SEO />
      <PrivacyLink />
    </>
  )
}
