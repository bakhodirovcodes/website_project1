import SEO from "@/components/SEO";
import SpecServices from "@/components/views/SpecServices/SpecialServices";

import React from "react";

const SpecialServices = () => {
  return (
    <>
      <SEO />
      <SpecServices />
    </>
  );
};

export default SpecialServices;
