import BuyForMe from "@/components/views/BuyForMe/BuyForMe"
import SEO from "@/components/SEO"

export default function BuyforMe() {
  return (
    <>
      <SEO />
      <BuyForMe />
    </>
  )
}
