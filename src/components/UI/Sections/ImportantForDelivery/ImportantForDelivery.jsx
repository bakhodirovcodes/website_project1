import AccordionInfo from "./AccordionInfo/AccordionInfo"
import { accordionData } from "./accordionData"
import styles from "./styles.module.scss"

const ImportantForDelivery = () => {
  return (
    <div className={styles.container}>
      <h1 className={styles.title}>Важно при оформлении доставки</h1>
      <div className={styles.accordion}>
        {accordionData.map((el) => (
          <AccordionInfo key={el.id} title={el.title} />
        ))}
      </div>
    </div>
  )
}

export default ImportantForDelivery
