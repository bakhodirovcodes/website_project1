import Typography from "@mui/material/Typography"
import { PlusIcon } from "/public/icons/icons"
import MuiAccordion from "@mui/material/Accordion"
import MuiAccordionSummary from "@mui/material/AccordionSummary"
import { styled } from "@mui/material/styles"
import MuiAccordionDetails from "@mui/material/AccordionDetails"
import AccordChild from "./AccordionChilde/AccordionChild"
import useObjects from "@/services/getObjectList"
import { accordionChildData } from "./accordionChildData"
import styles from "./styles.module.scss"

const Accordion = styled((props) => (
  <MuiAccordion disableGutters elevation={0} square {...props} />
))({
  borderBottom: `1px solid #D5DADD`,
  "&:not(:last-child)": {
    borderBottom: 0,
  },
  "&:before": {
    display: "none",
  },
})

const AccordionSummary = styled((props) => (
  <MuiAccordionSummary expandIcon={<PlusIcon />} {...props} />
))({
  "& .MuiAccordionSummary-expandIconWrapper.Mui-expanded": {
    transform: "rotate(0)",
  },
})

const AccordionDetails = styled(MuiAccordionDetails)({
  borderTop: "1px solid #D5DADD",
})

const AccordionInfo = (props) => {
  const { object } = useObjects({
    table_slug: "faq",
    objectParams: { data: {} },
    objectProperties: {
      enabled: true,
    },
  })
  const { title, key } = props
  return (
    <div className={styles.container}>
      <Accordion
        key={key}
        sx={{
          ".css-z6ttdw-MuiPaper-root-MuiAccordion-root:last-of-type": {
            borderRadius: 0,
          },
        }}
      >
        <AccordionSummary
          expandIcon={<PlusIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography className={styles.title}>{title}</Typography>
        </AccordionSummary>
        <AccordionDetails>
          {accordionChildData.map((item) => (
            <AccordChild key={item.id} title={item.title} text={item.text} />
          ))}
        </AccordionDetails>
      </Accordion>
    </div>
  )
}

export default AccordionInfo
