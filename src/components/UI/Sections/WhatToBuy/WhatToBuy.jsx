import { useForm } from "react-hook-form"
import { useState } from "react"
import { useRouter } from "next/router"
import { Controller } from "react-hook-form"
import { TextField } from "@mui/material"
import { styled } from "@mui/material/styles"
import { QuestionIcon, DeleteIcon, GreenPlusIcon } from "/public/icons/icons"
import InputAdornment from "@mui/material/InputAdornment"
import HoverElement from "./HoverElement/HoverElement"
import ErrorCard from "../../Cards/ErrorCard/ErrorCard"
import Button from "../../Buttons/Button"
import styles from "./styles.module.scss"

const CTextField = styled(TextField)({
  "& label.Mui-focused": {
    color: "#303940",
  },

  "& .MuiOutlinedInput-root": {
    "& fieldset": {
      borderColor: "#B0BABF",
    },
    "&:hover fieldset": {
      borderColor: "#B0BABF",
    },
    "&.Mui-focused fieldset": {
      borderColor: "#B0BABF",
    },
  },
})

const WhatToBuy = () => {
  const router = useRouter()
  const { handleSubmit, control } = useForm({
    defaultValues: {
      insert_link: "",
      product: "",
    },
  })

  const onSubmit = (data) => {
    const value = { insert_link: data.insert_link, product: data.product }
    router.push({
      pathname: "/ordershop",
      query: value,
    })
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className={styles.container}>
        <h1 className={styles.title}>Что купить?</h1>
        <div className={styles.items}>
          <Controller
            control={control}
            name="insert_link"
            render={({ field: { onChange, value }, fieldState: { error } }) => (
              <CTextField
                className={styles.firstInput}
                value={value}
                onChange={(e) => onChange(e.target.value)}
                label="Вставьте ссылку"
                placeholder="Скопируйте ссылку из магазина и вставьте сюда"
                sx={{ m: 1, width: "100%" }}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end" className={styles.quession}>
                      <QuestionIcon />
                      <div className={styles.hoverElement}>
                        <HoverElement />
                      </div>
                    </InputAdornment>
                  ),
                }}
              />
            )}
          />
          <Controller
            control={control}
            name="product"
            defaultValue=""
            render={({ field: { onChange, value }, fieldState: { error } }) => (
              <CTextField
                className={styles.secondInput}
                value={value}
                onChange={(e) => onChange(e.target.value)}
                label="Товар"
                placeholder="Введите имя товара"
                sx={{ m: 1, width: "100%" }}
              />
            )}
          />

          <div className={styles.deleteBtn}>
            <DeleteIcon />
          </div>
        </div>
        <div className={styles.error}>
          <ErrorCard />
        </div>
        <p className={styles.addItem}>
          <span>
            <GreenPlusIcon />
          </span>
          Добавить товар
        </p>
        <div className={styles.btn}>
          <Button type="submit" changeBorder={true} title="Продолжить" />
        </div>
      </div>
    </form>
  )
}

export default WhatToBuy
