import useTranslation from "next-translate/useTranslation"

import styles from "./styles.module.scss"

const BlogCards = ({ img, title, text }) => {
  const { t } = useTranslation()
  return (
    <div className={styles.box}>
      <div className={styles.img}>
        <img src={img} alt="img" />
      </div>
      <div className={styles.info}>
        <h2>{t(title)}</h2>
        <p>{t(text)}</p>
      </div>
    </div>
  )
}

export default BlogCards
