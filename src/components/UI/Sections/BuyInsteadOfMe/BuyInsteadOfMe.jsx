import useTranslation from "next-translate/useTranslation"

import { blogData } from "./blogData"
import BlogCards from "./BlogCards/BlogCards"
import styles from "./styles.module.scss"

const BuyInsteadOfMe = () => {
  const { t } = useTranslation()
  return (
    <div className={styles.container} id="blog">
      <h2 className={styles.title}>
        {t("interesting")} <br />
        {t("quesBuildWall")}
      </h2>
      <p className={styles.text}>{t("ourRoad")}</p>

      <div className={styles.blogs}>
        {blogData.map((el) => (
          <BlogCards key={el.id} img={el.img} title={el.title} text={el.text} />
        ))}
      </div>
    </div>
  )
}

export default BuyInsteadOfMe
