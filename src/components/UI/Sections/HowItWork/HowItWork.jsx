import OutlinedButton from "../../Buttons/OutlinedBtn"
import Button from "../../Buttons/Button"
import useObjects from "@/services/getObjectList"
import styles from "./styles.module.scss"

const HowItWork = () => {
  const { object } = useObjects({
    table_slug: "how_it_work",
    objectParams: { data: {} },
    objectProperties: {
      enabled: true,
    },
  })
  return (
    <div className={styles.container}>
      <div className={styles.container__howItWork}>
        <h1 className={styles.title}>
          {object?.data?.data?.response[0]?.titile}
        </h1>
        <p className={styles.text}>
          {object?.data?.data?.response[0]?.description}
        </p>
        <div className={styles.buttons}>
          <Button changeBorder={true} title="Начать шоппинг" />
          <OutlinedButton title="calculateTheCost" />
        </div>
      </div>
      <div className={styles.container__video}>
        <video
          src={object?.data?.data?.response[0]?.video}
          loop
          playsInline
          autoPlay
          muted
        ></video>
      </div>
    </div>
  )
}

export default HowItWork
