import useTranslation from "next-translate/useTranslation"

import styles from "./styles.module.scss"

const AboutCard = ({ img, title, text }) => {
  const { t } = useTranslation()
  return (
    <div className={styles.card}>
      <div className={styles.imgs}>
        <img src={img} alt="cardImg" />
      </div>
      <h1>{t(title)}</h1>
      <p>{t(text)}</p>
    </div>
  )
}

export default AboutCard
