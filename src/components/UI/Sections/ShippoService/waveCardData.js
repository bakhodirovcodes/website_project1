export const cardData = [
  {
    id: 1,
    img: "/images/cardimg/mt-1584-home-img4.jpg",
    title: "meetingExp",
    text: "duringWork",
  },
  {
    id: 2,
    img: "/images/cardimg/mt-1584-home-img5.jpg",
    title: "hightlight",
    text: "weUseBest",
  },
  {
    id: 3,
    img: "/images/cardimg/mt-1584-home-img6.jpg",
    title: "addedValue",
    text: "weStandFor",
  },
  {
    id: 4,
    img: "/images/cardimg/mt-1584-home-img7.jpg",
    title: "safety",
    text: "safeWork",
  },
]
