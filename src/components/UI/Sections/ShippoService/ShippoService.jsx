import useTranslation from "next-translate/useTranslation"

import AboutCard from "./AboutCard/AboutCard"
import { cardData } from "./waveCardData"
import styles from "./styles.module.scss"

const ShippoService = () => {
  const { t } = useTranslation()
  return (
    <div className={styles.container} id="about">
      <h2>{t("about")}</h2>
      <p>{t("buildWall")}</p>
      <div className={styles.cards}>
        {cardData.map((el) => (
          <AboutCard key={el.id} img={el.img} title={el.title} text={el.text} />
        ))}
      </div>
    </div>
  )
}
export default ShippoService
