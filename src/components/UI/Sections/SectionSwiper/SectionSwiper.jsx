import useTranslation from "next-translate/useTranslation"

import styles from "./styles.module.scss"

const MainBanner = () => {
  const { t } = useTranslation()
  return (
    <div className={styles.sliderWrapper}>
      <div className={styles.box}>
        <h1>{t("weStand")}</h1>
        <p>{t("weAreProud")}</p>
      </div>
    </div>
  )
}

export default MainBanner
