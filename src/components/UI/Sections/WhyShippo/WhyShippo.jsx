import { cardsItems } from "./data"
import Cards from "./Cards/Cards"
import styles from "./styles.module.scss"

const WhyShippo = () => {
  return (
    <div className={styles.container} id="ourcompany">
      <div className={styles.details}>
        {cardsItems.map((el) => (
          <Cards key={el.id} img={el.img} name={el.name} number={el.number} />
        ))}
      </div>
    </div>
  )
}

export default WhyShippo
