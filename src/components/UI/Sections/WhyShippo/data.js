export const cardsItems = [
  {
    id: 1,
    img: "/images/cardsDetailsImg/mt-1584-home-icon10.png",
    number: "100+",
    name: "project",
  },
  {
    id: 2,
    img: "/images/cardsDetailsImg/mt-1584-home-icon11.png",
    number: "3+",
    name: "year",
  },
  {
    id: 3,
    img: "/images/cardsDetailsImg/mt-1584-home-icon-12.png",
    number: "100+",
    name: "stuff",
  },
  {
    id: 4,
    img: "/images/cardsDetailsImg/mt-1584-home-icon-13.png",
    number: "100+",
    name: "machineries",
  },
]
