import useTranslation from "next-translate/useTranslation"

import styles from "./cards.module.scss"

const Cards = (props) => {
  const { img, number, name } = props
  const { t } = useTranslation()
  return (
    <div className={styles.card}>
      <div className={styles.img}>
        <img src={img} alt="img" />
      </div>
      <div className={styles.content}>
        <span>{number}</span>
        <p className={styles.name}>{t(name)}</p>
      </div>
    </div>
  )
}

export default Cards
