import AdventureCard from "../../Cards/AdventureCard/AdventureCard"
import { adventureData } from "./adventureData"
import styles from "./styles.module.scss"

const AdventureOfService = () => {
  return (
    <div className={styles.container}>
      <h1 className={styles.title}>Преимущество этой услуги</h1>
      <div className={styles.cards}>
        {adventureData.map((el) => (
          <AdventureCard key={el.id} icons={el.icon} text={el.title} />
        ))}
      </div>
    </div>
  )
}

export default AdventureOfService
