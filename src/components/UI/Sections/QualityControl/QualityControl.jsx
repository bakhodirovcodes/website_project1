import QualityFom from "./QualityForm/QualityForm"
import styles from "./styles.module.scss"

const QualityControl = () => {
  return (
    <div className={styles.container}>
      <img
        className={styles.image}
        src="/images/qualityControl.png"
        alt="qualityImage"
      />
      <div className={styles.form}>
        <QualityFom />
      </div>
    </div>
  )
}

export default QualityControl
