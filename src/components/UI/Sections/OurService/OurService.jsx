import { useState } from "react"
import LeftContent from "./LeftContent/LeftContent"
import RightContent from "./RightContent/RightContent"
import styles from "./styles.module.scss"

const OurService = () => {
  const [index, setIndex] = useState(0)
 
  return (
    <div className={styles.container}>
      <h1 className={styles.container__serviceTitle}>Наши сервис-центры</h1>
      <div className={styles.contents}>
        <div className={styles.leftContent}>
          <LeftContent setIndex={setIndex} />
        </div>
        <div className={styles.rightContent}>
          <RightContent index={index} />
        </div>
      </div>
    </div>
  )
}

export default OurService
