import { serviceData } from "./serviceData"
import ServiceCenters from "@/components/UI/Sections/OurService/LeftContent/ServiceCenters/ServiceCenters"
import styles from "./left.module.scss"
import useObjects from "@/services/getObjectList"

const LeftContent = ({ setIndex }) => {
  const { object } = useObjects({
    table_slug: "service_centre",
    objectParams: { data: {} },
    objectProperties: {
      enabled: true,
    },
  })

  return (
    <div className={styles.container}>
      <div className={styles.container__content}>
        {object?.data?.data?.response?.map((item, ind) => (
          <ServiceCenters
            setIndex={setIndex}
            ind={ind}
            key={item.guid}
            image={item.photo}
            title={item.city}
            address={item.address}
            date={item.schedule}
          />
        ))}
      </div>
    </div>
  )
}

export default LeftContent
