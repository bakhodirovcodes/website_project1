export const serviceData = [
  {
    id: 1,
    image: "/images/serviceImages/serviceImg1.png",
    title: "Ташкент",
    address: "Мирабадский р-н, ул. Янги Куйлюк, 28",
    date: "Пн-Пт, от 10:00 до 20:00",
  },
  {
    id: 2,
    image: "/images/serviceImages/serviceImg2.png",
    title: "Ташкент",
    address: "Мирабадский р-н, ул. Янги Куйлюк, 28",
    date: "Пн-Пт, от 10:00 до 20:00",
  },
]
