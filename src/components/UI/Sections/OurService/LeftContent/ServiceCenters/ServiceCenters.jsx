import styles from "./service.module.scss"

const ServiceCenters = (props) => {
  const { image, title, address, date, setIndex, ind } = props
  return (
    <div onClick={() => setIndex(ind)} className={styles.container}>
      <div className={styles.container__image}>
        <img src={image} alt="serviceImg" />
      </div>
      <div className={styles.info}>
        <h2 className={styles.info__title}>{title}</h2>
        <p className={styles.info__address}>{address}</p>
        <p className={styles.info__date}>{date}</p>
      </div>
    </div>
  )
}
export default ServiceCenters
