import styles from "./right.module.scss"
import useObjects from "@/services/getObjectList"

const RightContent = ({ index }) => {
  const { object } = useObjects({
    table_slug: "service_centre",
    objectParams: { data: {} },
    objectProperties: {
      enabled: true,
    },
  })
  return (
    <div className={styles.container}>
      <iframe
        src={object?.data?.data?.response?.[index].location}
        title="map"
        style={{ border: "transparent" }}
      />
    </div>
  )
}

export default RightContent
