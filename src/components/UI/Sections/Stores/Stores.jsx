import StoresCards from "./StoresCards/StoresCards"
import OutlinedButton from "../../Buttons/OutlinedBtn"
import useObjects from "@/services/getObjectList"
import { useRouter } from "next/router"
import styles from "./styles.module.scss"

const Stores = () => {
  const router = useRouter()
  const { object } = useObjects({
    table_slug: "contact_shops",
    objectParams: { data: {} },
    objectProperties: {
      enabled: true,
    },
  })
  console.log("oo", object)
  return (
    <div className={styles.container}>
      <h1>Популярные магазины в Турции</h1>
      <div className={styles.stores}>
        {object?.data?.data?.response?.map((el) => (
          <StoresCards key={el.guid} images={el.photo} texts={el.name} />
        ))}
      </div>
      <div className={styles.btn}>
        <OutlinedButton
          onClick={() => router.push("/stores")}
          title="viewAll"
        />
      </div>
    </div>
  )
}

export default Stores
