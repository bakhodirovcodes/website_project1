import styles from "./styles.module.scss"
import { useRouter } from "next/router"
import useObjects from "@/services/getObjectList"

const WhatWeDo = (props) => {
  const router = useRouter()
  const { object } = useObjects({
    table_slug: "about_us",
    objectParams: { data: {} },
    objectProperties: {
      enabled: true,
    },
  })
  const { data } = useObjects({
    table_slug: "buy_for_me",
    objectParams: { data: {} },
    objectProperties: {
      enabled: true,
    },
  })

  const { title } = props
  return (
    <div className={styles.container}>
      <div className={styles.video}>
        <video
          src={
            router.route === "/buyForMe"
              ? data?.data?.data?.response[0]?.video
              : object?.data?.data?.response[0]?.video
          }
          loop
          playsInline
          autoPlay
          muted
        />
      </div>
      <div className={styles.content}>
        {router.route === "/buyForMe" ? (
          <h1>{data?.data?.data?.response[0]?.Title}</h1>
        ) : (
          <h1>{object?.data?.data?.response[0]?.title}</h1>
        )}
        {router.route === "/buyForMe" ? (
          <p>{data?.data?.data?.response[0]?.description}</p>
        ) : (
          <p>{object?.data?.data?.response[0]?.description}</p>
        )}
      </div>
    </div>
  )
}

export default WhatWeDo
