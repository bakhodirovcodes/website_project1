import AdviceCard from "../../Cards/AdviceCard/AdviceCard"
import { adviceData } from "./adviceData"
import styles from "./styles.module.scss"

const Advices = () => {
  return (
    <div className={styles.container}>
      {adviceData.map((el) => (
        <AdviceCard
          key={el.id}
          icon={el.icon}
          title={el.title}
          text={el.text}
        />
      ))}
    </div>
  )
}

export default Advices
