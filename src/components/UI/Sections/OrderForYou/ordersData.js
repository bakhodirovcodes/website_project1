import {
  LinkIcon,
  DestinationIcon,
  OrderShoppingIcon,
} from "/public/icons/icons"
import Arrows from "./Arrows/Arrows"

export const ordersData = [
  {
    id: 1,
    icon: <LinkIcon />,
    title: "Отправьте нам ссылку",
    component: <Arrows />,
  },
  {
    id: 2,
    icon: <OrderShoppingIcon />,
    title: "Мы осуществим покупку.",
    component: <Arrows />,
  },
  {
    id: 3,
    icon: <DestinationIcon />,
    title: "Получи посылку в Казахстане",
  },
]
