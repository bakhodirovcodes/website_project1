import useTranslation from "next-translate/useTranslation"

import { contentData } from "./contentData"
import Content from "./Content/Content"
import styles from "./styles.module.scss"

const DeliveryCount = () => {
  const { t } = useTranslation()
  return (
    <div className={styles.container} id="details">
      <h2 className={styles.title}>
        {t("whatDoes")} <br />
        {t("theCopetiores")}
      </h2>
      <p className={styles.text}>{t("buildWallSrives")}</p>

      <div className={styles.content}>
        {contentData.map((el) => (
          <Content key={el.id} img={el.img} title={el.title} text={el.text} />
        ))}
      </div>
    </div>
  )
}

export default DeliveryCount
