import useTranslation from "next-translate/useTranslation"

import styles from "./styles.module.scss"

const Content = ({ title, img, text }) => {
  const { t } = useTranslation()
  return (
    <div className={styles.container}>
      <div className={styles.img}>
        <img src={img} alt="img" />
      </div>
      <div className={styles.info}>
        <h2 className={styles.title}>{t(title)}</h2>
        <p className={styles.text}>{t(text)}</p>
      </div>
    </div>
  )
}

export default Content
