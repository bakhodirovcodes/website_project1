import ContentRu from "./ContentRu/ContentRu"
import useObjects from "@/services/getObjectList"
import styles from "./styles.module.scss"

const TermSection = () => {
  const { object } = useObjects({
    table_slug: "general_terms",
    objectParams: { data: {} },
    objectProperties: {
      enabled: true,
    },
  })

  return (
    <div className={styles.container}>
      <h1 className={styles.title}>Общие условия</h1>
      {object?.data?.data?.response?.map((item) => (
        <p
          key={item.guid}
          className={styles.text}
          dangerouslySetInnerHTML={{ __html: item.description }}
        ></p>
      ))}
    </div>
  )
}

export default TermSection
