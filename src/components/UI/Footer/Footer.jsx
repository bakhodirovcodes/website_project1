import Link from "next/link"
import { Container } from "@mui/material"
import useTranslation from "next-translate/useTranslation"
import {
  ResursIcon,
  LocationIcon,
  EmailIcon,
  PhoneIcon,
  FacebookIcon,
  TelegramIcon,
  InstagramIcon,
  WatsappIcon,
  TwitterIcon,
} from "/public/icons/icons"
import VisaCards from "../Cards/VisaCard/VisaCards"
import { cards } from "./cards"
import styles from "./style.module.scss"

export function Footer() {
  const { t } = useTranslation("common")
  return (
    <footer className={styles.footer}>
      <Container>
        <div className={styles.box}>
          <div className={styles.links}>
            <div className={styles.infoAbout}>
              <img src="/images/yunusbekStarBuilding.png" alt="img" />
            </div>
            <div className={styles.infoWays}>
              <img src="/images/mt-1584-blog-img4.jpg" alt="img" />
              <p className={styles.text}>{t("theMostCommon")}</p>
            </div>
            <ul className={styles.urls}>
              <li>
                <Link href="/">
                  <a className={styles.urlItems}>{t("about")}</a>
                </Link>
              </li>
              <li>
                <Link href="/">
                  <a className={styles.urlItems}>{t("details")}</a>
                </Link>
              </li>
              <li>
                <Link href="/">
                  <a className={styles.urlItems}>{t("ourcompany")}</a>
                </Link>
              </li>
              <li>
                <Link href="/">
                  <a className={styles.urlItems}>{t("blog")}</a>
                </Link>
              </li>
            </ul>
            <ul className={styles.contacts}>
              <li>
                <Link href="/">
                  <a>
                    <LocationIcon />
                    <span>Ташкент, улица Пушкина 12</span>
                  </a>
                </Link>
              </li>
              <li>
                <Link href="/">
                  <a>
                    <EmailIcon />
                    <span>info@shippo.uz</span>
                  </a>
                </Link>
              </li>
              <li>
                <Link href="/">
                  <a>
                    <PhoneIcon />
                    <span>+7 727 344 14 64</span>
                  </a>
                </Link>
              </li>
            </ul>
          </div>
        </div>
        <div className={styles.underline}></div>
        <div className={styles.footerText}>
          <p className={styles.info}>{t("footerPoliceText")}</p>
          <ul>
            <li className={styles.networks}>
              <Link href="/">
                <a>
                  <FacebookIcon />
                </a>
              </Link>
              <Link href="/">
                <a>
                  <InstagramIcon />
                </a>
              </Link>
              <Link href="/">
                <a>
                  <TwitterIcon />
                </a>
              </Link>
              <Link href="/">
                <a>
                  <TelegramIcon />
                </a>
              </Link>
              <Link href="/">
                <a>
                  <WatsappIcon />
                </a>
              </Link>
            </li>
          </ul>
        </div>
      </Container>
    </footer>
  )
}
