import styles from "./styles.module.scss"
import { AlcoIcon } from "/public/icons/icons"

const ProhibitedCard = (props) => {
  const { icon, title, text } = props

  return (
    <div className={styles.container}>
      <div className={styles.container__leftItem}>
        <span className={styles.icon}>
          <AlcoIcon />
        </span>
      </div>
      <div className={styles.container__content}>
        <h1 className={styles.title}>{title}</h1>
        <p className={styles.text}>{text}</p>
      </div>
    </div>
  )
}

export default ProhibitedCard
