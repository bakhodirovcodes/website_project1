import Link from "next/link"
import styles from "./styles.module.scss"
import { destroyCookie } from "nookies"
import { useRouter } from "next/router"

const ProfilePopover = (props) => {
  const router = useRouter()

  const handleLogout = () => {
    destroyCookie(null, "token", { path: "/" })
    destroyCookie(null, "user_name", { path: "/" })
    destroyCookie(null, "user_id", { path: "/" })
    router.push("/")
    // window.location.reload();
  }
  const { icon, title, number, link } = props
  return (
    <Link href={link}>
      <a
        className={styles.container}
        onClick={title === "Выйти" && handleLogout}
      >
        {icon}
        <span className={styles.text}>{title}</span>
        {number && <span className={styles.count}>{number}</span>}
      </a>
    </Link>
  )
}

export default ProfilePopover
