import { useState } from "react"
import OutlinedButton from "../../Buttons/OutlinedBtn"
import Modals from "../../Modal/Modal"
import InviteFrends from "@/components/views/InviteFrends/InviteFrends"
import styles from "./styles.module.scss"

const data = [
  {
    id: 1,
    title: "Ваш баланс",
    text: "97 $",
    button: "Пополнить баланс",
  },
  {
    id: 2,
    title: "Бонус",
    text: "17 $",
    button: "Списать бонусы",
  },
]

const BonusPopover = () => {
  const [open, setOpen] = useState(false)
  const handleOpen = () => setOpen(true)
  const handleClose = () => setOpen(false)

  return (
    <div className={styles.container}>
      {data.map((el) => (
        <div className={styles.balance} key={el.id}>
          <div className={styles.balanceCount}>
            <p className={styles.countText}>{el.title}</p>
            <p className={styles.count}>{el.text}</p>
          </div>
          <button type="button">{el.button}</button>
        </div>
      ))}
      <div className={styles.content}>
        <h1 className={styles.bonuceTitle}>Пригласи друга и получай бонусы</h1>
        <OutlinedButton
          onClick={handleOpen}
          title="Пригласить друга"
          className={styles.outlined}
          textCLassName={styles.text}
        />
        <Modals width="60%" open={open} handleClose={handleClose}>
          <InviteFrends handleClose={handleClose} />
        </Modals>
      </div>
    </div>
  )
}

export default BonusPopover
