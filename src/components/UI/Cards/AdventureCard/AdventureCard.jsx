import styles from "./styles.module.scss"

const AdventureCard = (props) => {
  const { icons, text } = props
  return (
    <div className={styles.card}>
      <div className={styles.icon}>{icons}</div>
      <p className={styles.text}>{text}</p>
    </div>
  )
}

export default AdventureCard
