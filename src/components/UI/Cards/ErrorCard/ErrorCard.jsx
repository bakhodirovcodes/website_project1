import { ErrorInfoRoundedIcon } from "public/icons/icons"
import styles from "./styles.module.scss"

const ErrorCard = () => {
  return (
    <div className={styles.conatiner}>
      <div className={styles.icon}>
        <ErrorInfoRoundedIcon />
      </div>
      <div className={styles.text}>
        <p>
          Данный магазин в настоящий момент не доступен в рамках услуги. Мы уже
          оформили заявку на открытие доступа для этого магазина. Наши
          сотрудники в течение 1 рабочего дня обработают заявку. В случае
          активации сайта Вы получите письмо с подтверждением на свой адрес
          электронной почты. Вы также можете отправить запрос по адресу
          purchasing@globbing.com.
        </p>
      </div>
    </div>
  )
}

export default ErrorCard
