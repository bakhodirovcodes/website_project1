import styles from "./wave.module.scss"

const WaveCard = (props) => {
  const { icon, title, text } = props
  return (
    <div className={styles.container}>
      <div className={styles.icon}>{icon}</div>
      <h1 className={styles.title}>{title}</h1>
      <p className={styles.text}>{text}</p>
    </div>
  )
}

export default WaveCard
