import { Drawer } from "@mui/material"
import Link from "next/link"
import useTranslation from "next-translate/useTranslation"
import { useRouter } from "next/router"
import { makeStyles } from "@mui/styles"
import OutlinedButton from "../../Buttons/OutlinedBtn"
import { LogoIcon, CloseIcon } from "/public/icons/icons"
import styles from "./styles.module.scss"

export const useStyles = makeStyles({
  root: {
    background: "rgba(0, 0, 0, 0.1)",
    "& .MuiPaper-root": {
      width: "50%",

      ["@media (max-width: 768px)"]: {
        width: "100%",
      },
    },
    "& .MuiOutlinedInput-notchedOutline": {
      border: "1px solid rgba(0, 0, 0, 0.12) !important",
    },
  },
})

const DrawerItem = ({ open, onClose }) => {
  const classes = useStyles()
  const { t } = useTranslation()
  const router = useRouter()

  const windowScrollTo = (event, name, yOffset) => {
    event.preventDefault()

    if (typeof window !== "undefined") {
      const el = document.querySelector(name)
      const y = el
        ? el.getBoundingClientRect().top + window.pageYOffset + yOffset
        : 0
      window.scrollTo({ top: y, behavior: "smooth" }) // Use smooth scrolling
    }
  }

  return (
    <Drawer
      open={open}
      onClose={onClose}
      anchor="right"
      className={classes.root}
    >
      <div className={styles.box}>
        <div className={styles.header}>
          <Link href="/">
            <a>
              <img src="/images/yunusbekStarBuilding.png" alt="img" />
            </a>
          </Link>
          <div onClick={onClose}>
            <CloseIcon />
          </div>
        </div>
        <div className={styles.boxBody}>
          <nav>
            <ul>
              <li>
                <Link href="/#about">
                  <a
                    onClick={(event) => {
                      windowScrollTo(event, "#about", 0), onClose()
                    }}
                  >
                    About us
                  </a>
                </Link>
              </li>
              <li>
                <Link href="/#details">
                  <a
                    onClick={(event) => {
                      windowScrollTo(event, "#details", 50), onClose()
                    }}
                  >
                    Details
                  </a>
                </Link>
              </li>
              <li>
                <Link href="/#ourcompany">
                  <a
                    onClick={(event) => {
                      windowScrollTo(event, "#ourcompany", 0), onClose()
                    }}
                  >
                    Our company
                  </a>
                </Link>
              </li>
              <li>
                <Link href="/#blog">
                  <a
                    onClick={(event) => {
                      windowScrollTo(event, "#blog", 40), onClose()
                    }}
                  >
                    Blog
                  </a>
                </Link>
              </li>
            </ul>
          </nav>
          <div className={styles.menuItems}>
            <h2 className={styles.menuTexts}>{t("language")}</h2>
            <div className={styles.button}>
              <Link href={router.asPath} locale="ru">
                <a>
                  <OutlinedButton
                    className={
                      router.locale === "ru" ? styles.changeColor : styles.btn
                    }
                    textCLassName={styles.text}
                    title={t("russian")}
                  />
                </a>
              </Link>
              <Link href={router.asPath} locale="en">
                <a>
                  <OutlinedButton
                    className={
                      router.locale === "en" ? styles.changeColor : styles.btn
                    }
                    textCLassName={styles.text}
                    title={t("english")}
                  />
                </a>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </Drawer>
  )
}

export default DrawerItem
