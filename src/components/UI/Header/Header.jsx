import { useState, useEffect } from "react"
import { useRouter } from "next/router"
import { Container, Select, MenuItem } from "@mui/material"
import useTranslation from "next-translate/useTranslation"
import Headroom from "react-headroom"
import Link from "next/link"
import { LogoIcon } from "/public/icons/icons"
import { styled } from "@mui/material/styles"
import InputBase from "@mui/material/InputBase"
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown"
import DrawerItem from "./Drawer/DrawerItem"
import { MenuIcon } from "public/icons/icons"
import { PhoneIphone } from "@mui/icons-material"
import { AccessTime } from "@mui/icons-material"
import styles from "./style.module.scss"

const BootstrapInput = styled(InputBase)(({ theme }) => ({
  "label + &": {
    marginTop: theme.spacing(3),
  },
  "& .MuiInputBase-input": {
    borderRadius: 5,
    position: "relative",
    backgroundColor: "transparent",
    border: "1px solid #000",
    fontSize: 14,
    color: "#000",
    padding: "10px 12px 10px 16px",
    "&:focus": {
      borderRadius: 5,
    },
    ".MuiSvgIcon": {
      color: "red",
    },
  },
}))

const Header = () => {
  const [selectedValue, setselectedValue] = useState("")
  const [isScrolled, setIsScrolled] = useState(false)
  const [openDrawer, setOpenDrawer] = useState(false)
  const handleOpenDrawer = () => setOpenDrawer(true)
  const handleCloseDrawer = () => setOpenDrawer(false)
  const router = useRouter()
  const { t } = useTranslation()
  const langs = [
    {
      key: "ru",
      label: "Ру",
    },
    {
      key: "en",
      label: "En",
    },
  ]
  const handleChange = (event) => {
    setselectedValue(event.target.value)
  }

  const windowScrollTo = (event, name, yOffset) => {
    event.preventDefault()

    if (typeof window !== "undefined") {
      const el = document.querySelector(name)
      const y = el
        ? el.getBoundingClientRect().top + window.pageYOffset + yOffset
        : 0
      window.scrollTo({ top: y, behavior: "smooth" }) // Use smooth scrolling
    }
  }

  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY > 50) {
        setIsScrolled(true)
      } else {
        setIsScrolled(false)
      }
    }

    window.addEventListener("scroll", handleScroll)

    return () => {
      window.removeEventListener("scroll", handleScroll)
    }
  }, [])

  return (
    <header className={styles.header}>
      <Container>
        <div className={styles.box}>
          <Link href="/">
            <a className={styles.logo}>
              <img src="/images/yunusbekStarBuilding.png" alt="img" />
            </a>
          </Link>
          <div className={styles.times}>
            <div className={styles.customTime}>
              <PhoneIphone style={{ color: "#fe7012", width: "20px" }} />
              <div className={styles.customInfo}>
                <p>{t("customTimeText")}</p>
                <a href="tel:998943718000">+99894 371 8000</a>
              </div>
            </div>
            <span className={styles.line}></span>
            <div className={styles.customTime}>
              <AccessTime style={{ color: "#fe7012", width: "20px" }} />
              <div className={styles.customInfo}>
                <p>{t("workingTime")}</p>
                <a>{t("timeInt")}</a>
              </div>
            </div>
          </div>
          <Select
            className={styles.select}
            displayEmpty
            value={router.locale ? router.locale : selectedValue}
            onChange={handleChange}
            IconComponent={KeyboardArrowDownIcon}
            input={<BootstrapInput />}
          >
            <MenuItem sx={{ display: "none" }} value="" disabled>
              Ру
            </MenuItem>
            {langs.map((el) => (
              <MenuItem sx={{ display: "block" }} key={el.key} value={el.key}>
                <Link href={router.asPath} locale={el.key}>
                  <div>{el.label}</div>
                </Link>
              </MenuItem>
            ))}
          </Select>

          <div className={styles.menuBtn} onClick={handleOpenDrawer}>
            <MenuIcon />
          </div>
        </div>
      </Container>
      <div
        className={`${styles.navWrapper} ${isScrolled ? styles.active : ""}`}
      >
        <Container>
          <div className={styles.navItems}>
            <nav>
              <ul>
                <li>
                  <Link href="/#about">
                    <a onClick={(event) => windowScrollTo(event, "#about", 0)}>
                      {t("about")}
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href="/#details">
                    <a
                      onClick={(event) => windowScrollTo(event, "#details", 20)}
                    >
                      {t("details")}
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href="/#ourcompany">
                    <a
                      onClick={(event) =>
                        windowScrollTo(event, "#ourcompany", 0)
                      }
                    >
                      {t("ourcompany")}
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href="/#blog">
                    <a onClick={(event) => windowScrollTo(event, "#blog", 30)}>
                      {t("blog")}
                    </a>
                  </Link>
                </li>
              </ul>
            </nav>
          </div>
        </Container>
      </div>

      <DrawerItem open={openDrawer} onClose={handleCloseDrawer} />
    </header>
  )
}

export default Header
