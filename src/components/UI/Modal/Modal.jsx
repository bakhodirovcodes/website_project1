import Modal from "@mui/material/Modal"
import styles from "./styles.module.scss"

const Modals = (props) => {
  const { open, handleClose, children, width } = props
  return (
    <Modal open={open} onClose={handleClose}>
      <div style={{ width: `${width}` }} className={styles.container}>
        {children}
      </div>
    </Modal>
  )
}

export default Modals
