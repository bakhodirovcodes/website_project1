import { Container } from "@mui/material"
import styles from "./styles.module.scss"

const ColorValue = {
  1: styles.section,
  2: styles.change,
  3: styles.sectionSwiper,
  4: styles.swiper,
}

const CustomSections = (props) => {
  const { children, changeColor } = props
  return (
    <section className={ColorValue[changeColor]}>
      <Container>{children}</Container>
    </section>
  )
}

export default CustomSections
