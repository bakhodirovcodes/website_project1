import CustomSections from "../../CustomSections/CustomSection"
import FAQSection from "../../Sections/FAQSection/FAQSection"

const QuestionLink = () => {
  return (
    <>
      <CustomSections>
        <FAQSection />
      </CustomSections>
    </>
  )
}

export default QuestionLink
