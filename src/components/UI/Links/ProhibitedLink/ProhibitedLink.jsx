import CustomSections from "../../CustomSections/CustomSection"
import ProhibitedSection from "../../Sections/ProhibitedSection/ProhibitedSection"

const ProhibitedLink = () => {
  return (
    <>
      <CustomSections changeColor={1}>
        <ProhibitedSection />
      </CustomSections>
    </>
  )
}

export default ProhibitedLink
