import styles from "./otp.module.scss"
import VerificationInput from "react-verification-input"
import { Typography } from "@mui/material"
import { Controller } from "react-hook-form"

export default function OtpInput({
  defaultValue,
  name = "",
  control,
  seconds,
  id,
  label,
}) {
  return (
    <>
      <div className={styles.phoneBox}>
        {label && (
          <label htmlFor={id}>
            <Typography variant="variant='text_12_16_400'">{label}</Typography>
          </label>
        )}
        <Controller
          type="number"
          control={control}
          // rules={validation}
          defaultValue={defaultValue}
          name={name}
          render={({ field: { value, onChange, name } }) => {
            return (
              <VerificationInput
                removeDefaultStyles
                autoFocus={!value && true}
                placeholder=""
                type="number"
                validChars="0-9"
                length={6}
                id={id}
                inputProps={{ inputMode: "numeric" }}
                inputMode="numeric"
                onChange={onChange}
                value={value}
                classNames={{
                  container: `${styles.container}`,
                  character: `${styles.character}`,
                  characterInactive: `${styles.characterInactive}`,
                  characterSelected: `${styles.characterSelected}`,
                }}
              />
            )
          }}
        />
      </div>
      {/* <div className={styles.timeCounter}>
               <p>СМС отправлен</p>
               <p>00:{seconds < 10 ? '0' : ''}{seconds}</p>
            </div> */}
    </>
  )
}
