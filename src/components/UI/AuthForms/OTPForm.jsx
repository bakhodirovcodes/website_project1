import styles from "./style.module.scss"
import OtpInput from "./OtpInput/OtpInput"
import OutlinedButton from "../Buttons/OutlinedBtn"
import { useForm } from "react-hook-form"
import { useState } from "react"
import { setCookie } from "nookies"
import verifyEmailService from "@/services/Auth/veryfyEmail"
import objectService from "@/services/objectService"

const OTPForm = ({ setOpenCode, setOpenRegister, responseSendSms }) => {
  const [isLoading, setIsLoading] = useState(false)
  const { handleSubmit, control } = useForm({
    defaultValues: {
      code: "",
      user_found: responseSendSms?.data?.user_found === true,
    },
  })

  const onSubmit = (data) => {
    setIsLoading(true)
    const otp = data.code
    //  setOpenCode(false)
    //  setOpenRegister(true)

    verifyEmailService
      .verifyEmail(responseSendSms.sms_id, otp, {
        data: responseSendSms.data,
      })
      .then((response) => {
        if (responseSendSms?.data?.user_found) {
          objectService
            .getByIdUser(responseSendSms?.data?.user_id)
            .then((res) => {
              if (response?.data?.data?.token?.access_token) {
                objectService
                  .getByIdUser(response.data.data.user_id)
                  .then((resUser) => {
                    setCookie(
                      null,
                      "token",
                      response?.data?.data?.token?.access_token,
                      {
                        path: "/",
                      }
                    )
                    setCookie(
                      null,
                      "user_name",
                      resUser?.data?.response?.name || "ALIAS-NAME!",
                      {
                        path: "/",
                      }
                    )
                    setCookie(null, "user_id", resUser?.data?.response?.guid, {
                      path: "/",
                    })
                    window.location.reload()
                  })
              } else {
                setOpenRegister(true)
              }
              setOpenCode(false)
            })
        } else {
          data.user_found = false
          verifyEmailService
            .verifyEmail(responseSendSms?.sms_id, otp, {
              data: { ...data, user: responseSendSms?.user_found },
            })
            .then((res) => {
              setOpenCode(false)
              setOpenRegister(true)
            })
            .catch((err) => console.log(err))
        }
      })
  }

  return (
    <div className={styles.otpBox}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <h1 className={styles.title}>Вход в систему</h1>
        <p className={styles.text}>
          Коды подтверждения с кодом отправили на InteresyVlogs@gmail.com
          <span>Изменить</span>
        </p>

        <div className={styles.otpinputBox}>
          <OtpInput name="code" control={control} />
          <OutlinedButton
            type="submit"
            className={styles.btn}
            textCLassName={styles.text}
            title="Продолжать"
          />
          {/* <MainButton type="submit" text="Войти" fullWidth /> */}
        </div>
      </form>
    </div>
  )
}

export default OTPForm
