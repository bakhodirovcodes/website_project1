import { useForm } from "react-hook-form"
import { useState } from "react"
import Frow from "@/components/UI/FormElements/FRow"
import CTextField from "@/components/UI/FormElements/CTextField"
import OutlinedButton from "../Buttons/OutlinedBtn"
import { useDispatch } from "react-redux"
import { setOpenFalse } from "@/store/user/userSlice"
import { useSendCodeMutation } from "@/services/verifyEmailService"
import styles from "./style.module.scss"

const LoginForm = ({
  setOpen,
  setOpenCode,
  setSmsId,
  setEmail,
  setResponseSendSms,
}) => {
  const [isLoading, setIsLoading] = useState(false)
  const dispatch = useDispatch()

  const { handleSubmit, control } = useForm({
    defaultValues: {
      email: "",
      client_type: "WEB USER",
    },
  })

  const { mutate: sendCode } = useSendCodeMutation({
    onSuccess: (res) => {
      console.log(res)
      setResponseSendSms(res?.data?.data)
      setSmsId(res?.data?.data?.sms_id)
      setOpen(false)
      setOpenCode(true)
      dispatch(setOpenFalse(false))
    },
  })

  const onSubmit = (data) => {
    setIsLoading(true)
    setEmail(data.email)

    sendCode(data)
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)} className={styles.loginBox}>
      <h1 className={styles.title}>Вход в систему</h1>
      <div className={styles.inputBox}>
        <Frow label="Email">
          <CTextField
            fullWidth
            control={control}
            name="email"
            placeholder="Введите"
          />
        </Frow>
        <OutlinedButton
          type="submit"
          className={styles.btn}
          textCLassName={styles.text}
          title="Войты"
        />
      </div>
    </form>
  )
}

export default LoginForm
