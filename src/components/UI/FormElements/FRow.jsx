import styles from "./styles.module.scss"

const Frow = (props) => {
  const {
    label,
    children,
    required,
    changeLabel,
  } = props
  return (
    <div className={styles.Frow}>
      <div className={styles.label || changeLabel}>
        {label} {required && <span className={styles.requiredStart}>*</span>}
      </div>
      <div className={styles.component}>{children}</div>
    </div>
  )
}

export default Frow
