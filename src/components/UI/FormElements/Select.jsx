import ReactSelect from "react-select"

const aliasOptions = [
  { value: "option1", label: "Option 1" },
  { value: "option2", label: "Option 2" },
  { value: "option3", label: "Option 3" },
  { value: "option4", label: "Option 4" },
]

const Select = (props) => {
  const { options } = props

  const styles = {
    indicatorSeparator: () => null,
    control: (provided, state) => ({
      ...provided,
      padding: "0 4px 0 2px",
      border: "none",
      borderRadius: "8px",
      backgroundColor: "#F6F8F9",
    }),
  }

  return (
    <ReactSelect
      styles={styles}
      options={options || aliasOptions}
    />
  )
}

export default Select
