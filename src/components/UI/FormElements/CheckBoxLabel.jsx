import FormGroup from "@mui/material/FormGroup"
import FormControlLabel from "@mui/material/FormControlLabel"
import Checkbox from "@mui/material/Checkbox"

const CheckBoxLabel = (props) => {
  const { label, key } = props

  return (
    //  <FormGroup>
    <FormControlLabel
      key={key}
      control={
        <Checkbox
          key={key}
          sx={{
            color: "#D5DADD",
            padding: "6px",
            "&.Mui-checked": {
              color: "#EF722E",
            },
            "&.MuiFormGroup-root css-dmmspl-MuiFormGroup-root": {
              padding: 0,
            },
          }}
        />
      }
      label={label}
      sx={{
        padding: 0,
        marginLeft: "-7px",
        width: "100%",
        ".css-ahj2mt-MuiTypography-root": {
          fontSize: "16px",
          color: "#48535B",
        },
      }}
    />
    //  </FormGroup>
  )
}

export default CheckBoxLabel
