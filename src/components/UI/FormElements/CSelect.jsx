import {
  FormControl,
  FormHelperText,
  InputLabel,
  MenuItem,
  Select,
} from "@mui/material"
import { Controller } from "react-hook-form"
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown"

const CSelect = ({
  control,
  name,
  label,
  width = "100%",
  options = [],
  disabledHelperText,
  placeholder,
  required = false,
  defaultOption,
  selectedValue,
  rules = {},
  ...props
}) => {
  return (
    <Controller
      control={control}
      name={name}
      defaultValue=""
      rules={{
        required: required ? "Это обязательное поле" : false,
        ...rules,
      }}
      render={({ field: { onChange, value }, fieldState: { error } }) => (
        <FormControl style={{ width }}>
          <InputLabel size="small">{label}</InputLabel>
          <Select
            value={value || ""}
            label={label}
            size="small"
            error={error}
            inputProps={{ placeholder }}
            IconComponent={KeyboardArrowDownIcon}
            fullWidth
            displayEmpty
            onChange={(e) => {
              onChange(e.target.value)
            }}
            {...props}
          >
            {/* <MenuItem disabled value="">
              <p>{defaultOption}</p>
            </MenuItem> */}
            <MenuItem value="">
              <p>{selectedValue}</p>
            </MenuItem>
            {options?.map((option) => (
              <MenuItem
                key={option.value}
                value={option.value}
                sx={{ paddingY: "10px" }}
              >
                {option.label}
              </MenuItem>
            ))}
          </Select>
          {!disabledHelperText && (
            <FormHelperText error>{error?.message ?? " "}</FormHelperText>
          )}
        </FormControl>
      )}
    ></Controller>
  )
}

export default CSelect
