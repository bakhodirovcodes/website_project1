import React from "react";
import cls from "./styles.module.scss";
import SuperSwitch from "../SuperSwitch/SuperSwitch";

const SpecServicesCard = () => {
  return (
    <div className={cls.main}>
      <div className={cls.leftSide}>
        <img src="icons/boxIcon.svg" alt="box" className={cls.image} />
        <div className={cls.infoSection}>
          <h3 className={cls.infoTitle}>Задержать на складе</h3>
          <div className={cls.priceSection}>
            <select className={cls.select}>
              <option value="test">test</option>
            </select>
            <p className={cls.price}>4$</p>
          </div>
        </div>
      </div>

      <SuperSwitch />
    </div>
  );
};

export default SpecServicesCard;
