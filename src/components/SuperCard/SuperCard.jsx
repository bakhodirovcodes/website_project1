import { Container } from "@mui/system";
import React from "react";
import cls from "./styles.module.scss";

const SuperCard = ({
  title = "",
  header = "",
  mt = "28px",
  mb = "16px",
  children,
}) => {
  return (
    <Container>
      {header && <h1 className={cls.header}>{header}</h1>}
      <div
        className={cls.superCard}
        style={{ marginTop: mt, marginBottom: mb }}
      >
        <h2 className={cls.cardTitle}>{title}</h2>
        <div className={cls.cardChildren}>{children}</div>
      </div>
    </Container>
  );
};

export default SuperCard;
