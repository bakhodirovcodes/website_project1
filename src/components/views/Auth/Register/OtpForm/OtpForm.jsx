// import styles from "./styles.module.scss"
// import OtpInput from "./OtpInput/OtpInput"
// import OutlinedButton from "@/components/UI/Buttons/OutlinedBtn"
// import { useForm } from "react-hook-form"

// const OtpForm = ({ handleClose, handleOpenRegister }) => {
//   const { handleSubmit, control, reset } = useForm({
//     defaultValues: {
//       code: "",
//     },
//   })

//   const onSubmit = (data) => {
//     console.log(data)
//     handleClose(false)
//     handleOpenRegister(true)
//   }

//   return (
//     <div className={styles.loginBox}>
//       <form onSubmit={handleSubmit(onSubmit)}>
//         <h1 className={styles.title}>Вход в систему</h1>
//         <p className={styles.text}>
//           Коды подтверждения с кодом отправили на InteresyVlogs@gmail.com
//           <span>Изменить</span>
//         </p>

//         <div className={styles.inputBox}>
//           <OtpInput name="code" control={control} />
//           <OutlinedButton
//             type="submit"
//             className={styles.btn}
//             textCLassName={styles.text}
//             title="Продолжать"
//           />
//           {/* <MainButton type="submit" text="Войти" fullWidth /> */}
//         </div>
//       </form>
//     </div>
//   )
// }

// export default OtpForm
