// import { useForm } from "react-hook-form"
// import Frow from "@/components/UI/FormElements/FRow"
// import CTextField from "@/components/UI/FormElements/CTextField"
// import { styled } from "@mui/material/styles"
// import MuiCheckbox from "@mui/material/Checkbox"
// import OutlinedButton from "@/components/UI/Buttons/OutlinedBtn"
// import { SupergIcon } from "public/icons/icons"
// import { Modal } from "@mui/material"
// import styles from "./styles.module.scss"
// import { useRouter } from "next/router"

// const Checkbox = styled(MuiCheckbox)({
//   color: "#EF722E",
//   "&.Mui-checked": {
//     color: "#EF722E",
//   },
// })

// const LeftSide = (props) => {
//   const router = useRouter()
//   const {
//     title,
//     resetPassword,
//     forgotPassword,
//     outlinedText,
//     filledText,
//     handleOpen,
//   } = props
//   const { control, handleSubmit, reset } = useForm({
//     defaultValues: {
//       email: "",
//       password: "",
//       repeat_password: "",
//     },
//   })

//   const onSubmit = (values) => {
//     console.log("values", values)
//   }

//   return (
//     <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
//       <h1 className={styles.title}>{title}</h1>
//       <p className={styles.text}>
//         После этого вы получите свой адрес, который сможете использовать в
//         интернет-магазинах
//       </p>
//       <div className={styles.formInputs}>
//         <Frow label="Электронная почта" required>
//           <CTextField
//             fullWidth
//             control={control}
//             name="email"
//             placeholder="Электронная почта"
//             required
//           />
//         </Frow>
//         <Frow label="Пароль" required>
//           <CTextField
//             type="password"
//             fullWidth
//             control={control}
//             name="password"
//             placeholder="Создайте пароль"
//             required
//           />
//         </Frow>
//         {resetPassword && (
//           <Frow label="Повторите пароль" required>
//             <CTextField
//               type="password"
//               fullWidth
//               control={control}
//               name="repeat_password"
//               placeholder="Цвет товара"
//               required
//             />
//           </Frow>
//         )}
//       </div>

//       {forgotPassword ? (
//         <div className={styles.forgotPassword}>
//           <div className={styles.checkbox}>
//             <Checkbox /> Запомнить меня
//           </div>
//           <p className={styles.forgotPasswordText}>Забыли пароль?</p>
//         </div>
//       ) : (
//         <div className={styles.checkboxItem}>
//           <Checkbox /> Я принимаю <a>условия и положения</a>
//         </div>
//       )}
//       <div className={styles.buttons}>
//         <OutlinedButton
//           onClick={
//             outlinedText === "Создать аккаунт"
//               ? () => router.push("/register")
//               : () => router.push("/login")
//           }
//           className={styles.outlined}
//           textCLassName={styles.text}
//           title={outlinedText}
//         />
//         <OutlinedButton
//           onClick={
//             filledText === "Создать аккаунт"
//               ? handleOpen
//               : () => router.push("/")
//           }
//           type="submit"
//           className={styles.btn}
//           textCLassName={styles.text}
//           title={filledText}
//         />
//       </div>
//       <div className={styles.google}>
//         <button type="button" className={styles.googleBtn}>
//           <span>
//             <SupergIcon />
//           </span>
//           Войти через Google аккаунт
//         </button>
//       </div>
//     </form>
//   )
// }

// export default LeftSide
