import MuiPaper from "@mui/material/Paper";
import MuiTableCell, { tableCellClasses } from "@mui/material/TableCell";
import MuiTableRow from "@mui/material/TableRow";
import { styled } from "@mui/material/styles";

export const Paper = styled(MuiPaper)({
  border: "1px solid #E5E9EB",
  borderRadius: 6,
  boxShadow: "none",
});

export const TableRow = styled(MuiTableRow)({
  "& td, th": {
    border: "1px solid #E5E9EB",
  },
  "& th": {
    borderTop: 0,
    whiteSpace: "nowrap",
  },
  "& th:first-child, td:first-child": {
    borderLeft: 0,
  },
  "& th:last-child, td:last-child": {
    borderRight: 0,
  },
  "&:last-child td": {
    borderBottom: 0,
  },
});

export const TableCell = styled((props) => <MuiTableCell {...props} />)({
  [`&.${tableCellClasses.head}`]: {
    fontWeight: 600,
    fontSize: "0.875rem",
    lineHeight: "1.375rem",
    color: "#1A2024",
  },
  [`&.${tableCellClasses.body}`]: {
    padding: "0.75rem 1rem",
    fontSize: "0.875rem",
    lineHeight: "1.5rem",
    color: "#303940",
  },
});

export const TableData = {
  columns: [
    {
      label: "Наименование",
      id: 1,
    },
    {
      label: "Жидкость",
      id: 2,
    },
    {
      label: "Количество",
      id: 3,
    },
    {
      label: "Заметка",
      id: 4,
    },
    {
      label: "Стоимость",
      id: 5,
    },
    {
      label: "Сумма",
      id: 6,
    },
  ],
};
