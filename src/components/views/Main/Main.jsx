import CustomSections from "../../UI/CustomSections/CustomSection"
import WhyShippo from "../../UI/Sections/WhyShippo/WhyShippo"
import BuyInsteadOfMe from "../../UI/Sections/BuyInsteadOfMe/BuyInsteadOfMe"
import Stores from "../../UI/Sections/Stores/Stores"
import DeliveryCount from "../../UI/Sections/DeliveryCount/DeliveryCount"
import ShippoService from "../../UI/Sections/ShippoService/ShippoService"
import MainBanner from "../../UI/Sections/SectionSwiper/SectionSwiper"

export function Main() {
  return (
    <main>
      <MainBanner />
      <CustomSections changeColor={2}>
        <ShippoService />
      </CustomSections>
      <DeliveryCount />
      <WhyShippo />
      <BuyInsteadOfMe />
    </main>
  )
}
