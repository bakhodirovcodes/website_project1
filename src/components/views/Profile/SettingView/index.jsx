import { styled } from "@mui/material/styles"
import MuiButton from "@mui/material/Button"
import { AddIcon } from "public/icons/icons2"
import Infos from "./components/Infos"
import Notification from "./components/Notification"
import styles from "./styles.module.scss"

const Button = styled(MuiButton)({
  padding: "0.5rem 1.5rem",
  backgroundColor: "#EF722E",
  boxShadow: "none",
  fontSize: "1rem",
  lineHeight: "1.5rem",
  color: "#FFF",
  "&:hover": {
    backgroundColor: "#EF722E",
  },
})

function SettingView() {
  return (
    <div className={styles.content}>
      <h2 className={styles.title}>Настройка</h2>
      <Infos />
      <Notification />
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <Button variant="contained" startIcon={<AddIcon />}>
          Сохранить
        </Button>
      </div>
    </div>
  )
}

export default SettingView
