import classNames from "classnames"
import { styled } from "@mui/material/styles"
import MuiTextField from "@mui/material/TextField"
import InputAdornment from "@mui/material/InputAdornment"
import MenuItem from "@mui/material/MenuItem"
import { ChevronDownIcon } from "public/icons/icons2"
import styles from "./styles.module.scss"

const TextField = styled((props) => (
  <MuiTextField
    select
    InputProps={{
      endAdornment: (
        <InputAdornment
          position="end"
          style={{
            position: "absolute",
            right: "7px",
            pointerEvents: "none",
          }}
        >
          <ChevronDownIcon />
        </InputAdornment>
      ),
    }}
    {...props}
  />
))({
  width: "100%",
  "& .MuiOutlinedInput-root": {
    padding: "0 0.75rem 0 0.75rem",
    borderRadius: "0.375rem",
    "& fieldset": {
      borderColor: "#E5E9EB",
    },
    "&:hover fieldset": {
      borderColor: "#E5E9EB",
    },
    "&.Mui-focused fieldset": {
      borderWidth: "1px",
      borderColor: "#E5E9EB",
    },
    "& .MuiSelect-select": {
      padding: "0.75rem 2rem 0.75rem 0",
      fontSize: "0.875rem",
      lineHeight: "1.125rem",
    },
    "& .MuiSvgIcon-root": {
      display: "none",
    },
  },
})

const infos = [
  {
    id: "info-01",
    label: "Имя",
    value: "Мереке",
  },
  {
    id: "info-02",
    label: "Фамилия",
    value: "Мамажонова",
  },
  {
    id: "info-03",
    label: "Отчество",
    value: "Мамажонова",
  },
  {
    id: "info-04",
    label: "Год рождение",
    value: "11/02/1995",
  },
  {
    id: "info-05",
    label: "Номер телефона",
    value: "+77054445558",
  },
]

const genders = [
  {
    id: "gender-01",
    label: "Женский",
    value: "female",
  },
  {
    id: "gender-02",
    label: "Мужской",
    value: "male",
  },
]

function Infos() {
  return (
    <div className={styles.infos}>
      <h4 className={styles.infos__title}>Личная информация</h4>

      <div className={styles.infos__wrapper}>
        {infos.map(({ id, label, value }) => (
          <div key={id} className={styles.infos__item}>
            <label className={styles.infos__label}>{label}</label>
            <div className={styles.infos__value}>{value}</div>
          </div>
        ))}
        <div className={styles.infos__item}>
          <label className={styles.infos__label}>Пол</label>
          <TextField defaultValue="female">
            {genders.map(({ id, label, value }) => (
              <MenuItem key={id} value={value}>
                {label}
              </MenuItem>
            ))}
          </TextField>
        </div>
      </div>

      <div
        className={classNames(
          styles.infos__wrapper,
          styles.infos__wrapper_twoColumn
        )}
      >
        <div className={styles.infos__item}>
          <label className={styles.infos__label}>Электронная почта</label>
          <div className={styles.infos__value}>
            merekeissakhmetova@gmail.com
          </div>
        </div>
        <div className={styles.infos__item}>
          <label className={styles.infos__label}>Пароль</label>
          <div className={styles.infos__value}>********</div>
        </div>
      </div>
    </div>
  )
}

export default Infos
