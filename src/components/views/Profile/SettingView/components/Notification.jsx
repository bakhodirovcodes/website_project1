import { styled } from "@mui/material/styles"
import MuiCheckbox from "@mui/material/Checkbox"
import styles from "./styles.module.scss"

const Checkbox = styled(MuiCheckbox)({
  color: "#EF722E",
  "&.Mui-checked": {
    color: "#EF722E",
  },
})

function Notification() {
  return (
    <div className={styles.notification}>
      <h4 className={styles.notification__title}>Уведомления</h4>

      <div className={styles.notification__wrapper}>
        <div className={styles.notification__wrapperItem}>
          <Checkbox />
          Уведомление
        </div>
        <div className={styles.notification__wrapperItem}>
          <Checkbox />
          Уведомлния о рассылке на почту
        </div>
      </div>
    </div>
  )
}

export default Notification
