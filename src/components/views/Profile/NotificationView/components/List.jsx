import { useRouter } from "next/router"
import styles from "./styles.module.scss"

function List({ data }) {
  const router = useRouter()

  return (
    <div className={styles.list}>
      {data.map(({ id, label, date }) => (
        <div
          key={id}
          className={styles.list__item}
          onClick={() => router.push(`${router.pathname}/${id}`)}
        >
          <span>{label}</span>
          <span className={styles.list__date}>{date}</span>
        </div>
      ))}
    </div>
  )
}

export default List
