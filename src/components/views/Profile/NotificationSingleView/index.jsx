import Header from "./components/Header"
import styles from "./styles.module.scss"

function NotificationSingleView() {
  return (
    <div className={styles.content}>
      <Header />

      <div className={styles.content__wrapper}>
        <p className={styles.content__title}>Уважаемый(ая) Мереке,</p>
        <p
          dangerouslySetInnerHTML={{
            __html: `Трек-номер 1ZW569040366826717
            Вес 1.2 kg
            Детали Футболка
            Ваша посылка уже на нашем международном складе и скоро будет готова к отправке. Пожалуйста, убедитесь, что статус Вашего получателя "Подтвержден" и, что все данные вашей посылки указаны.
            Спасибо, что Вы с нами.
            Команда Глоббинг Казахстан`,
          }}
          className={styles.content__text}
        />
      </div>
    </div>
  )
}

export default NotificationSingleView
