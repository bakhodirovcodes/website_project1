import { CopyIcon } from "public/icons/icons2"
import styles from "./styles.module.scss"

const data = [
  {
    id: "item-01",
    subtitle: "Номер трекинга",
    title: "2ZDC23MCSLD",
  },
  {
    id: "item-02",
    subtitle: "Получатель",
    title: "Мереке Мамажонова",
  },
]

function InfoCards() {
  return (
    <div className={styles.infoCards}>
      {data.map(({ id, subtitle, title }) => (
        <div key={id} className={styles.infoCards__card}>
          <div className={styles.infoCards__content}>
            <span className={styles.infoCards__subtitle}>{subtitle}</span>
            <span className={styles.infoCards__title}>{title}</span>
          </div>
          <div
            className={styles.infoCards__action}
            onClick={() => navigator.clipboard.writeText(title)}
          >
            <CopyIcon />
          </div>
        </div>
      ))}
    </div>
  )
}

export default InfoCards
