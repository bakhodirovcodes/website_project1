import { styled } from "@mui/material/styles"
import TableContainer from "@mui/material/TableContainer"
import Table from "@mui/material/Table"
import TableHead from "@mui/material/TableHead"
import TableBody from "@mui/material/TableBody"
import MuiTableCell, { tableCellClasses } from "@mui/material/TableCell"
import MuiTableRow from "@mui/material/TableRow"
import MuiPaper from "@mui/material/Paper"
import styles from "./styles.module.scss"

const Paper = styled(MuiPaper)({
  border: "1px solid #E5E9EB",
  borderRadius: 6,
  boxShadow: "none",
})

const TableRow = styled(MuiTableRow)({
  "& td, th": {
    border: "1px solid #E5E9EB",
  },
  "& th": {
    borderTop: 0,
  },
  "& th:first-child, td:first-child": {
    borderLeft: 0,
  },
  "& th:last-child, td:last-child": {
    borderRight: 0,
  },
  "&:last-child td": {
    borderBottom: 0,
  },
})

const TableCell = styled((props) => <MuiTableCell {...props} />)({
  [`&.${tableCellClasses.head}`]: {
    fontWeight: 600,
    fontSize: "0.875rem",
    lineHeight: "1.375rem",
    color: "#1A2024",
  },
  [`&.${tableCellClasses.body}`]: {
    padding: "0.75rem 1rem",
    fontSize: "0.875rem",
    lineHeight: "1.5rem",
    color: "#303940",
  },
})

function DeclarationTable({ data: { columns, rows } }) {
  return (
    <div className={styles.table}>
      <h4 className={styles.table__title}>Декларация</h4>

      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 700 }} aria-label="customized table">
          <TableHead>
            <TableRow>
              {columns.map(({ id, label }) => (
                <TableCell key={id}>{label}</TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map(({ id, name, quantity, note, price, amount }) => (
              <TableRow key={id}>
                <TableCell>{name}</TableCell>
                <TableCell>{quantity}</TableCell>
                <TableCell>{note}</TableCell>
                <TableCell>{price}</TableCell>
                <TableCell>{amount}</TableCell>
              </TableRow>
            ))}
            <TableRow>
              <TableCell colSpan={4} sx={{ fontWeight: 600 }}>
                Итого
              </TableCell>
              <TableCell>378 $</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  )
}

export default DeclarationTable
