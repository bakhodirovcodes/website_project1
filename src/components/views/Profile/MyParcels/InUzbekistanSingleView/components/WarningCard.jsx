import { WarningIcon } from "public/icons/icons2"
import styles from "./styles.module.scss"

function WarningCard() {
  return (
    <div className={styles.warningCard}>
      <span className={styles.warningCard__icon}>
        <WarningIcon />
      </span>
      <span>Пожалуйста, пополните баланс, чтобы получить посылку.</span>
    </div>
  )
}

export default WarningCard
