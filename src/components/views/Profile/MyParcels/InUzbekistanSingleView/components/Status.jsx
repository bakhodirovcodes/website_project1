import { styled } from "@mui/material/styles"
import MuiButton from "@mui/material/Button"
import classNames from "classnames"
import { ActiveIcon, PencileEditIcon, AddIcon } from "public/icons/icons2"
import styles from "./styles.module.scss"

const OutlinedButton = styled(MuiButton)({
  padding: "0.5rem 1.5rem",
  boxShadow: "none",
  border: "1px solid #DDE2E4",
  fontSize: "1rem",
  color: "#EF722E",
  lineHeight: "1.5rem",
  "&:hover": {
    borderColor: "#DDE2E4",
  },
})

const ContainedButton = styled(MuiButton)({
  padding: "0.5rem 1.5rem",
  backgroundColor: "#1AC19D",
  boxShadow: "none",
  fontSize: "1rem",
  color: "#FFF",
  lineHeight: "1.5rem",
  "&:hover": {
    backgroundColor: "#1AC19D",
    boxShadow: "none",
  },
})

const steps = [
  {
    id: "step-01",
    label: "Добавлено",
    date: "08.11.2022",
  },
  {
    id: "step-02",
    label: "Склад Турция",
    date: "08.11.2022",
  },
  {
    id: "step-03",
    label: "Упаковано для отправки",
    date: "08.11.2022",
  },
  {
    id: "step-04",
    label: "Аэропорт Турция",
    date: "08.11.2022",
  },
  {
    id: "step-05",
    label: "Склад в Узбекистане",
    date: "08.11.2022",
  },
  {
    id: "step-06",
    label: "Доставлено",
    date: "08.11.2022",
  },
]

function Status() {
  return (
    <div className={styles.status}>
      <h4 className={styles.status__title}>Отслеживание</h4>

      <div className={styles.status__progress}>
        {steps.map(({ id, label, date }, index) => (
          <div
            key={id}
            className={classNames(styles.status__step, {
              [styles.status__step_active]: index < 5,
            })}
          >
            <span className={styles.status__label}>{label}</span>
            <span className={styles.status__date}>{date}</span>
            {index < 5 && (
              <span className={styles.status__step_active_icon}>
                <ActiveIcon />
              </span>
            )}
          </div>
        ))}
      </div>

      <div className={styles.status__actions}>
        <OutlinedButton
          variant="outlined"
          startIcon={<PencileEditIcon fill="#EF722E" />}
        >
          Изменить
        </OutlinedButton>
        <ContainedButton variant="contained" startIcon={<AddIcon />}>
          Специальные услуги
        </ContainedButton>
      </div>
    </div>
  )
}

export default Status
