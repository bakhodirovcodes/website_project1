import { useRouter } from "next/router"
import {
  ProfileTableContainer,
  ProfileTable,
  ProfileTableHead,
  ProfileTableBody,
  ProfileTableRow,
  ProfileTableCell,
} from "@/components/ProfileTable"
import Stack from "@mui/material/Stack"
import { PencileEditIcon } from "public/icons/icons2"
import styles from "./styles.module.scss"

function Table({ data: { columns, rows } }) {
  const router = useRouter()

  return (
    <ProfileTableContainer>
      <ProfileTable>
        <ProfileTableHead>
          <ProfileTableRow>
            {columns.map(({ id, label }) => (
              <ProfileTableCell key={id}>{label}</ProfileTableCell>
            ))}
          </ProfileTableRow>
        </ProfileTableHead>
        <ProfileTableBody>
          {rows.map(({ id, trackCode, name, price, expectedDate }) => (
            <ProfileTableRow key={id}>
              <ProfileTableCell>{trackCode}</ProfileTableCell>
              <ProfileTableCell>{name}</ProfileTableCell>
              <ProfileTableCell>{price}</ProfileTableCell>
              <ProfileTableCell>{expectedDate}</ProfileTableCell>
              <ProfileTableCell style={{ width: 0 }}>
                <Stack direction="row" spacing="0.625rem">
                  <span
                    className={styles.table__delete}
                    onClick={() => router.push(`${router.pathname}/${id}`)}
                  >
                    <PencileEditIcon />
                  </span>
                </Stack>
              </ProfileTableCell>
            </ProfileTableRow>
          ))}
        </ProfileTableBody>
      </ProfileTable>
    </ProfileTableContainer>
  )
}

export default Table
