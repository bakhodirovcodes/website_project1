import { styled } from "@mui/material/styles"
import MuiButton from "@mui/material/Button"
import { InfoIcon, CopyIcon } from "public/icons/icons2"
import styles from "./styles.module.scss"

const Button = styled((props) => <MuiButton {...props} />)({
  width: "100%",
  padding: "0.75rem 1.5rem",
  boxShadow: "none",
  border: "1px solid #DDE2E4",
  fontSize: "1rem",
  color: "#EF722E",
  lineHeight: "1.5rem",
  "&:hover": {
    borderColor: "#DDE2E4",
  },
})

function InfoAndCopy() {
  return (
    <div className={styles.infoAndCopy}>
      <div className={styles.infoAndCopy__text}>
        <span className={styles.infoAndCopy__textIcon}>
          <InfoIcon />
        </span>
        <span>
          Введите этот адрес как адрес доставки при совершении онлайн покупок из
          магазинов Турция.
        </span>
      </div>

      <Button startIcon={<CopyIcon fill="#EF722E" />}>Скопировать все</Button>
    </div>
  )
}

export default InfoAndCopy
