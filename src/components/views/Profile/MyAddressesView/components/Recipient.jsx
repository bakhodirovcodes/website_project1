import { styled } from "@mui/material/styles"
import MuiTextField from "@mui/material/TextField"
import InputAdornment from "@mui/material/InputAdornment"
import MenuItem from "@mui/material/MenuItem"
import { ChevronDownIcon } from "public/icons/icons2"
import styles from "./styles.module.scss"

const TextField = styled((props) => (
  <MuiTextField
    select
    InputProps={{
      endAdornment: (
        <InputAdornment
          position="end"
          style={{
            position: "absolute",
            right: "7px",
            pointerEvents: "none",
          }}
        >
          <ChevronDownIcon />
        </InputAdornment>
      ),
    }}
    {...props}
  />
))({
  width: "calc(50% - 0.5rem)",
  "& .MuiOutlinedInput-root": {
    padding: "0 0.75rem 0 0.75rem",
    borderRadius: "0.375rem",
    "& fieldset": {
      borderColor: "#E5E9EB",
    },
    "&:hover fieldset": {
      borderColor: "#E5E9EB",
    },
    "&.Mui-focused fieldset": {
      borderWidth: "1px",
      borderColor: "#E5E9EB",
    },
    "& .MuiSelect-select": {
      padding: "0.75rem 2rem 0.75rem 0",
      fontSize: "0.875rem",
      lineHeight: "1.125rem",
    },
    "& .MuiSvgIcon-root": {
      display: "none",
    },
  },
})

const recipients = [
  {
    id: "recipient-01",
    label: "Mereke Mamajanova",
    value: "mereke-mamajanova-1",
  },
  {
    id: "recipient-02",
    label: "Mereke Mamajanova",
    value: "mereke-mamajanova-2",
  },
]

function Recipient() {
  return (
    <div className={styles.recipient}>
      <p className={styles.recipient__title}>Получатель</p>
      <TextField defaultValue="mereke-mamajanova-1">
        {recipients.map(({ id, label, value }) => (
          <MenuItem key={id} value={value}>
            {label}
          </MenuItem>
        ))}
      </TextField>
    </div>
  )
}

export default Recipient
