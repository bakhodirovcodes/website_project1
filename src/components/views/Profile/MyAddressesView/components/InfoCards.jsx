import InfoCard from "./InfoCard"
import styles from "./styles.module.scss"

function InfoCards({ data }) {
  return (
    <div className={styles.infoCards}>
      {data.map((item) => (
        <InfoCard key={data.id} data={item} />
      ))}
    </div>
  )
}

export default InfoCards
