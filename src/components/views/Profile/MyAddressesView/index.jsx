import Recipient from "./components/Recipient"
import InfoCards from "./components/InfoCards"
import InfoAndCopy from "./components/InfoAndCopy"
import { myAddressesInfoItems } from "../profileData"
import styles from "./styles.module.scss"

function MyAddressesView() {
  return (
    <div className={styles.content}>
      <h2 className={styles.title}>Мои адреса</h2>
      <Recipient />
      <InfoCards data={myAddressesInfoItems} />
      <InfoAndCopy />
    </div>
  )
}

export default MyAddressesView
