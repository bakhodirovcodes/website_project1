import { useState } from "react"
import OrderCard from "@/components/UI/Cards/OrderCard/OrderCard"
import { cardsItems, fildeItems, addedItems } from "./cardItems"
import { CloseIcon } from "public/icons/icons"
import UrlFilde from "./Fildes/UrlFilde/UrlFilde"
import AddFrendsFilde from "./Fildes/AddFrendsFilde/AddFrendsFilde"
import AddFrendsTable from "./AddFrendsTable/AddFrendsTable"
import styles from "./styles.module.scss"

const InviteFrends = ({ handleClose }) => {
  const [openTable, setOpenTable] = useState(false)
  console.log(openTable)

  const handleButton = (e) => {
    if (e.type === "click") {
      setOpenTable(true)
    } else {
      setOpenTable(false)
    }
  }

  return (
    <div className={styles.container}>
      <div onClick={handleClose} className={styles.close}>
        <CloseIcon />
      </div>
      <h1 className={styles.title}>Приглашай друзей и зарабатывай</h1>
      <div className={styles.orders}>
        {cardsItems.map((el) => (
          <OrderCard
            key={el.id}
            icon={el.icon}
            title={el.title}
            component={el.component}
          />
        ))}
      </div>
      <UrlFilde />
      <h1 className={styles.titleScore}>Правила зачисление баллов</h1>
      <p className={styles.text}>
        После получения первого заказа друга вы получите 4$ и 3% от стоимости
        доставки. Затем вам будет начисляться по 3% от каждой последующей его
        доставки.
      </p>
      <div className={styles.fildes}>
        {fildeItems.map((el) => (
          <AddFrendsFilde
            key={el.id}
            title={el.title}
            text={el.text}
            btnText={el.btnText}
            handleButton={handleButton}
          />
        ))}
      </div>
      {openTable && <AddFrendsTable data={addedItems} />}
    </div>
  )
}

export default InviteFrends
