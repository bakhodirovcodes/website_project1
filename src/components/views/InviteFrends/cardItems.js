import { OrderShoppingIcon, LocalIcon, AddIcon } from "/public/icons/icons"
import Arrows from "@/components/UI/Sections/OrderForYou/Arrows/Arrows"

export const cardsItems = [
  {
    id: 1,
    icon: <LocalIcon />,
    title: "Скопируйте и отправьте ссылку другу",
    component: <Arrows />,
  },
  {
    id: 2,
    icon: <AddIcon />,
    title: "Друг зарегистрируется по этой ссылке",
    component: <Arrows />,
  },
  {
    id: 1,
    icon: <OrderShoppingIcon />,
    title: "Начните получать бонусы",
  },
]

export const fildeItems = [
  {
    id: 1,
    title: "Приглашенных друзей",
    text: "661",
  },
  {
    id: 2,
    title: "Заработанный бонус",
    text: "3959.76 $",
    btnText: "Подробное",
  },
]

export const addedItems = {
  columns: [
    { id: "column-01", label: "Приглашенные друзья" },
    { id: "column-02", label: "ID номер" },
    { id: "column-03", label: "Заработанный бонус" },
  ],
  rows: [
    {
      id: "row-01",
      trackCode: "kalieva.anara79@gmail.com",
      name: "417901",
      price: "126 $",
    },
    {
      id: "row-02",
      trackCode: "kalieva.anara79@gmail.com",
      name: "417901",
      price: "126 $",
    },
    {
      id: "row-03",
      trackCode: "kalieva.anara79@gmail.com",
      name: "417901",
      price: "126 $",
    },
    {
      id: "row-04",
      trackCode: "kalieva.anara79@gmail.com",
      name: "417901",
      price: "126 $",
    },
    {
      id: "row-05",
      trackCode: "kalieva.anara79@gmail.com",
      name: "417901",
      price: "126 $",
    },
  ],
}
