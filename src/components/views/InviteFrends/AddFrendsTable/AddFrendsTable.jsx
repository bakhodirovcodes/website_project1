import styles from "./styles.module.scss"
import {
  ProfileTableContainer,
  ProfileTable,
  ProfileTableHead,
  ProfileTableBody,
  ProfileTableRow,
  ProfileTableCell,
} from "@/components/ProfileTable"

const AddFrendsTable = ({ data: { columns, rows } }) => {
  return (
    <ProfileTableContainer>
      <ProfileTable>
        <ProfileTableHead>
          <ProfileTableRow>
            {columns.map(({ id, label }) => (
              <ProfileTableCell key={id}>{label}</ProfileTableCell>
            ))}
          </ProfileTableRow>
        </ProfileTableHead>
        <ProfileTableBody>
          {rows.map(({ id, trackCode, name, price }) => (
            <ProfileTableRow key={id}>
              <ProfileTableCell>{trackCode}</ProfileTableCell>
              <ProfileTableCell>{name}</ProfileTableCell>
              <ProfileTableCell>{price}</ProfileTableCell>
            </ProfileTableRow>
          ))}
        </ProfileTableBody>
      </ProfileTable>
    </ProfileTableContainer>
  )
}

export default AddFrendsTable
