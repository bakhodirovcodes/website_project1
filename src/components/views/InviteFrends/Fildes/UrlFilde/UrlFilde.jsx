import { UrlCopyIcon } from "/public/icons/icons"
import styles from "./styles.module.scss"

const UrlFilde = () => {
  return (
    <div className={styles.container}>
      <div className={styles.leftContent}>
        <p className={styles.title}>Ссылка для приглашения</p>
        <p className={styles.url}>
          https://kz.globbing.com/ru/registration?affiliation_hash=5eef3a05d3ebb2p4q95qt=34t34
        </p>
      </div>
      <button
        className={styles.copyBtn}
        onClick={() =>
          navigator.clipboard.writeText(
            "https://kz.globbing.com/ru/registration?affiliation_hash=5eef3a05d3ebb2p4q95qt=34t34"
          )
        }
      >
        <UrlCopyIcon />
        Скопировать ссылку
      </button>
    </div>
  )
}

export default UrlFilde
