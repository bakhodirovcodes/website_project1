import styles from "./styles.module.scss"

const AddFrendsFilde = (props) => {
  const { title, text, btnText, handleButton } = props
  return (
    <div className={styles.container}>
      <div className={styles.leftContent}>
        <p className={styles.title}>{title}</p>
        <p className={styles.text}>{text}</p>
      </div>
      {btnText && (
        <div className={styles.rightSide}>
          <button onClick={(e) => handleButton(e)} className={styles.btn}>
            {btnText}
          </button>
        </div>
      )}
    </div>
  )
}

export default AddFrendsFilde
