import CustomSections from "@/components/UI/CustomSections/CustomSection"
import useTranslation from "next-translate/useTranslation"
import WhatWeDo from "@/components/UI/Sections/WhatWeDo/WhatWeDo"
import Brend from "@/components/UI/Sections/Brend/Brend"
import WithShippo from "@/components/UI/Sections/WithShippo/WithShippo"
import OurService from "@/components/UI/Sections/OurService/OurService"
import QualityControl from "@/components/UI/Sections/QualityControl/QualityControl"
import styles from "./styles.module.scss"

const About = () => {
  const { t } = useTranslation()

  return (
    <div className={styles.container}>
      <CustomSections>
        <h1>{t("about")}</h1>
      </CustomSections>
      <CustomSections>
        <WhatWeDo title="Что мы делаем ?" />
      </CustomSections>
      <CustomSections changeColor={2}>
        <Brend />
      </CustomSections>
      <CustomSections changeColor={1}>
        <WithShippo />
      </CustomSections>
      <CustomSections changeColor={2}>
        <QualityControl />
      </CustomSections>
      <CustomSections changeColor={2}>
        <OurService />
      </CustomSections>
    </div>
  )
}

export default About
