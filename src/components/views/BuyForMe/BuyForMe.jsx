import CustomSections from "@/components/UI/CustomSections/CustomSection"
import OrderForYou from "@/components/UI/Sections/OrderForYou/OrderForYou"
import WhatWeDo from "@/components/UI/Sections/WhatWeDo/WhatWeDo"
import WhatToBuy from "@/components/UI/Sections/WhatToBuy/WhatToBuy"
import AdventureOfService from "@/components/UI/Sections/AdventureOfService/AdventureOfService"
import Advices from "@/components/UI/Sections/Advices/Advices"
import styles from "./styles.module.scss"

const BuyForMe = () => {
  return (
    <div className={styles.container}>
      <CustomSections>
        <OrderForYou />
      </CustomSections>
      <CustomSections changeColor={1}>
        <WhatToBuy />
      </CustomSections>
      <CustomSections changeColor={3}>
        <WhatWeDo />
      </CustomSections>
      <CustomSections changeColor={1}>
        <AdventureOfService />
      </CustomSections>
      <CustomSections changeColor={3}>
        <Advices />
      </CustomSections>
    </div>
  )
}

export default BuyForMe
