import { Avatar } from "@mui/material"
import MenuItem from "./MenuItem"
import { menuItems } from "../profileData"
import styles from "./styles.module.scss"

function Sidebar() {
  return (
    <div className={styles.sidebar}>
      <div className={styles.sidebar__header}>
        <Avatar
          sx={{
            fontWeight: 600,
            fontSize: "0.875rem",
            color: "#ef722e",
            lineHeight: "1.5rem",
            background: "#ef722e14",
          }}
        >
          M
        </Avatar>

        <div className={styles.sidebar__info}>
          <h4 className={styles.sidebar__name}>Мереке Мамажонова</h4>
          <p className={styles.sidebar__email}>merekeissakhmetova@gmail.com</p>
        </div>
      </div>

      <div className={styles.sidebar__menu}>
        {menuItems.map((item) => (
          <MenuItem key={item.id} data={item} />
        ))}
      </div>
    </div>
  )
}

export default Sidebar
