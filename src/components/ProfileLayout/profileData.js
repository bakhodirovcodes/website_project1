import {
  ProfileMyParcelsIcon,
  ProfileNotificationIcon,
  ProfileMyAddressesIcon,
  ProfileBalanceAndTransactionsIcon,
  ProfileRecipientsIcon,
  ProfileSettingIcon,
} from "public/icons/icons2"

export const menuItems = [
  {
    id: "menu-item-01",
    title: "Мои посылки",
    icon: ProfileMyParcelsIcon,
    children: [
      {
        id: "item-child-01",
        title: "Добавлена",
        pathname: "/profile/my-parcels/added",
      },
      {
        id: "item-child-02",
        title: "На складе",
        pathname: "/profile/my-parcels/in-stock",
      },
      {
        id: "item-child-03",
        title: "В пути",
        pathname: "/profile/my-parcels/on-way",
      },
      {
        id: "item-child-04",
        title: "В Узбекистан",
        pathname: "/profile/my-parcels/in-uzbekistan",
      },
      {
        id: "item-child-05",
        title: "Получено",
        pathname: "/profile/my-parcels/received",
      },
    ],
  },
  {
    id: "menu-item-02",
    title: "Уведомление",
    pathname: "/profile/notification",
    icon: ProfileNotificationIcon,
  },
  {
    id: "menu-item-03",
    title: "Мои адреса",
    pathname: "/profile/my-addresses",
    icon: ProfileMyAddressesIcon,
  },
  {
    id: "menu-item-04",
    title: "Баланс и транзакции",
    pathname: "/profile/balance-and-transactions",
    icon: ProfileBalanceAndTransactionsIcon,
  },
  {
    id: "menu-item-05",
    title: "Получатели",
    pathname: "/profile/recipients",
    icon: ProfileRecipientsIcon,
  },
  {
    id: "menu-item-06",
    title: "Настройка",
    pathname: "/profile/setting",
    icon: ProfileSettingIcon,
  },
]
