import { styled } from "@mui/material/styles"
import TableContainer from "@mui/material/TableContainer"
import Table from "@mui/material/Table"
import TableHead from "@mui/material/TableHead"
import TableBody from "@mui/material/TableBody"
import MuiTableCell, { tableCellClasses } from "@mui/material/TableCell"
import MuiTableRow from "@mui/material/TableRow"
import MuiPaper from "@mui/material/Paper"

const Paper = styled(MuiPaper)({
  border: "1px solid #E5E9EB",
  borderRadius: 6,
  boxShadow: "none",
})

const TableRow = styled((props) => <MuiTableRow {...props} />)({
  "& td, th": {
    border: "1px solid #E5E9EB",
  },
  "& th": {
    borderTop: 0,
  },
  "& th:first-child, td:first-child": {
    borderLeft: 0,
  },
  "& th:last-child, td:last-child": {
    borderRight: 0,
  },
  "&:last-child td": {
    borderBottom: 0,
  },
})

const TableCell = styled((props) => <MuiTableCell {...props} />)({
  [`&.${tableCellClasses.head}`]: {
    fontWeight: 600,
    fontSize: "0.875rem",
    lineHeight: "1.375rem",
    color: "#1A2024",
  },
  [`&.${tableCellClasses.body}`]: {
    padding: "0.75rem 1rem",
    fontSize: "0.875rem",
    lineHeight: "1.5rem",
    color: "#303940",
  },
})

export function ProfileTableContainer(props) {
  return (
    <TableContainer component={Paper} {...props}>
      {props.children}
    </TableContainer>
  )
}

export function ProfileTable(props) {
  return (
    <Table sx={{ minWidth: 700 }} aria-label="profile table" {...props}>
      {props.children}
    </Table>
  )
}

export function ProfileTableHead(props) {
  return <TableHead {...props}>{props.children}</TableHead>
}

export function ProfileTableBody(props) {
  return <TableBody {...props}>{props.children}</TableBody>
}

export function ProfileTableRow(props) {
  return <TableRow {...props}>{props.children}</TableRow>
}

export function ProfileTableCell(props) {
  return <TableCell {...props}>{props.children}</TableCell>
}
