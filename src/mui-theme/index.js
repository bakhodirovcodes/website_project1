// https://mui.com/material-ui/customization/theming/

import { createTheme } from "@mui/material"
import { rem } from "@/utils/pxToRem"

export default createTheme({
  components: {
    MuiContainer: {
      styleOverrides: {
        root: {
          paddingLeft: rem(10),
          paddingRight: rem(10),
          "@media (min-width:1440px)": {
            maxWidth: "1300px",
            width: "100%",
          },
          "@media (min-width:1200px)": {
            paddingLeft: rem(10),
            paddingRight: rem(10),
          },
        },
      },
    },

    MuiDialog: {
      styleOverrides: {
        paper: {
          maxWidth: "none",
        },
      },
    },
  },
})
